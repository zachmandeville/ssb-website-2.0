// aesthetic master sheet!!!!
export const shellShadow = '#6F74E5';
export const shellColour = '#C1CDF0';
export const hermiepatchwork = '#B7FDB8';
export const hermiepatchworkShadow = '#45B754';
export const hermiepatchbay = '#F4D7EE';
export const hermiepatchbayShadow = '#FFAAEE';
export const buttonColour = '#FEFC00';
export const backgroundColour = '#CDD7F4';
export const titleColour = '#818CEB';
